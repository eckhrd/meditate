using Toybox.Application as App;

class MeditationApp extends App.AppBase {

    hidden var timer;
    hidden var stored_time;
    
    function initialize() {
        timer = new MeditationTimer();
    }

    //! onStart() is called on application start up
    function onStart() {
        stored_time = getProperty("meditation time");
        if (stored_time != null) {
            timer.setMeditationTime(stored_time);
        } else {
            timer.setMeditationTime(900);
        } 
        clearProperties();
    }

    //! onStop() is called when your application is exiting
    function onStop() {
        setProperty("meditation time", timer.getMeditationTime(:seconds));
    }

    //! Return the initial view of your application here
    function getInitialView() {
        return [ new MeditationView(timer), new MeditationDelegate(timer) ];
    }
}