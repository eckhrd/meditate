using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Attention as Attention;


class MeditationView extends Ui.View {
    hidden var my_timer;
    hidden var remaining_time_text;
    
    function initialize(timer) {
        my_timer = timer;
        remaining_time_text = timer.getRemainingTime(:string);
    }

    //! Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.MainLayout(dc));
    }

    //! Called when this View is brought to the foreground. Restore
    //! the state of this View and prepare it to be shown. This includes
    //! loading resources into memory.
    function onShow() {
    }

    //! Update the view
    function onUpdate(dc) {
        // Call the parent onUpdate function to redraw the layout.
        View.onUpdate(dc);
        
        // Set foreground and background colors.
        dc.setColor( Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT );
        
        // Display remaining time
        dc.drawText( 0.5 * dc.getWidth(),
                     0.1 * dc.getHeight(),
                     Gfx.FONT_NUMBER_HOT,
                     my_timer.getRemainingTime(:string),
                     Gfx.TEXT_JUSTIFY_CENTER
                   );
    }

    //! Called when this View is removed from the screen. Save the
    //! state of this View here. This includes freeing resources from
    //! memory.
    function onHide() {
    }
}


class MeditationDelegate extends Ui.BehaviorDelegate {

    hidden var vibrate_data;
    hidden var my_timer;
    
    function initialize(timer) {
        my_timer     = timer;
        vibrate_data = [ new Attention.VibeProfile(  75, 100 ) ];
    }
    
    function timerCallback() {
        if (my_timer.getRemainingTime(:seconds) > 0 ) {
            my_timer.decRemainingTime(1);
            Ui.requestUpdate();
        } else {
            my_timer.stop();
            Attention.vibrate( vibrate_data );
        }
    }
        
    function onKey(key) {
        if ( key.getKey() == Ui.KEY_ENTER) {
            if ( my_timer.isRunning() ) {
                Attention.vibrate( vibrate_data );
                my_timer.stop();
            } else {
                Attention.vibrate( vibrate_data );
                my_timer.start( method(:timerCallback), 1000, true );
            }
        }
    }
    
    function onTap() {
        var np = new Ui.NumberPicker( Ui.NUMBER_PICKER_TIME, my_timer.getMeditationTime(:picker) );
        var npd = new MeditationTimePickerDelegate(my_timer);
        Ui.pushView( np, npd, Ui.SLIDE_IMMEDIATE );
    }
}


class MeditationTimePickerDelegate extends Ui.NumberPickerDelegate {

    hidden var my_timer;

    function initialize(timer) {
        my_timer = timer;
    }
    
    
    function onNumberPicked(value) {
        // .value() returns the selected time 'value' in seconds.
        my_timer.setMeditationTime( value.value() );
        Ui.requestUpdate();
    }
}
