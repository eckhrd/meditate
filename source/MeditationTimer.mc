using Toybox.Lang as Lang;
using Toybox.Time.Gregorian as Calendar;
using Toybox.Timer as Timer;

class MeditationTimer extends Timer.Timer {

    hidden var meditation_time;
    hidden var remaining_time;
    hidden var running;
        
    function initialize() {
        running = false;
        setMeditationTime(900);
    }
    
    function getRemainingTime(format) {
        if (format == :string) {
            return Lang.format( "$1$:$2$", [ (remaining_time/60).format("%02d"),
                                             (remaining_time%60).format("%02d") ]
                              );
        }
        else if (format == :seconds) {
            return remaining_time;
        }
        
        else {
            return null;
        }
    }
    
    function setMeditationTime(value) {
        meditation_time = value;
        remaining_time = value;
    }
    
    function getMeditationTime(format) {
        if (format == :seconds) {
            return meditation_time;
        } else if (format == :picker){
            return Calendar.duration( { :hours=>meditation_time/3600,
                                        :minutes=>meditation_time%3600/60,
                                        :seconds=>meditation_time%3600%60 }
                                    );
        } else {
           return null;
        }
    }
    
    function decRemainingTime(value) {
        remaining_time -= value;
    }
    
    function start(callback, time, repeat) {
        Timer.Timer.start(callback, time, repeat);
        running = true;
    }
    
    function stop() {
        Timer.Timer.stop();
        running = false;
    }
    
    function isRunning() {
        return running;
    }
}