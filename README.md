# README

Meditate is a simple meditation timer for the Garmin vívoactive smart watch. You can adjust the duration of your meditation session by tapping the screen and start the timer by pressing the right button of your vivoactive.

You can [download](https://apps.garmin.com/en-US/apps/0035884a-5806-4cc7-a317-d7dd1b93e709) the App from Garmin's Connect IQ Store.

The app background image and icon is derived from Jens Tärning's [Meditation icon](https://thenounproject.com/term/meditation/51613/) as provided on The Noun Project under the [Creative Commons 3.0 license](http://creativecommons.org/licenses/by/3.0/us/).
